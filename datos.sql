﻿select sol.id, est.nombre_establecimiento, sol_req.id, cat.descripcion, sol_req_det.id, rub.descripcion, 
res.id, coalesce(res.avance_intervencion,0) as avance_intervencion,
res_det.id, coalesce(res_det.factor_intervencion,0) as factor_intervencion,
sit.id
from solicitudes_intervenciones sol 
join solicitudes_intervenciones_requerimientos sol_req on (sol_req.solicitud_intervencion_id = sol.id)
join catalogos_requerimientos_rubros sol_req_det on (sol_req_det.solicitud_intervencion_requerimiento_id = sol_req.id)
join establecimientos est on (est.id = sol.establecimiento_id)
join catalogos_fiscalizaciones cat on (cat.id = sol_req.catalogo_fiscalizacion_id)
join rubros rub on (rub.id = sol_req_det.rubro_id)
-- cada registro de avance
left join fiscalizaciones_resultados res on (res.solicitud_intervencion_requerimiento_id = sol_req.id)
left join fiscalizaciones_resultados_detalles res_det on (res_det.fiscalizacion_resultado_id = res.id 
	and res_det.catalogo_requerimiento_rubro_id = sol_req_det.id)
left join situaciones_intervenciones sit on (sit.id = res_det.situacion_intervencion_id)
where sol.persona_fiscalizador_id = 1
order by est.id, cat.id, res.id, rub.id, res_det.id
