/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.framework.security;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.METHOD, ElementType.TYPE})
@Documented
@Inherited
public @interface Require {
	
    public static final String DEFAULT_KEY = "DEFAULT";
    
    String value() default DEFAULT_KEY;
	
	String permiso() default DEFAULT_KEY;
	String rol() default  DEFAULT_KEY;
}
