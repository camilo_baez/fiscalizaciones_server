package py.framework.security;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.google.common.collect.ImmutableMap;

import py.fiscalizaciones.domain.Usuario;

@Path("/login")
public class AuthenticationEndpoint {

	@Inject
	SessionInfo infoSession;

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes("application/x-www-form-urlencoded")
	public Response authenticateUser(@FormParam("username") String username, @FormParam("password") String password) {
		try {
			Usuario toRet = infoSession.authenticate(username, password);
			return Response.ok().entity(toRet).build();
		} catch (Exception e) {
			e.printStackTrace();
			return Response.status(Response.Status.UNAUTHORIZED)
					.entity(ImmutableMap.of("error", "Fallo de autenticación.", "detail", e.getMessage())).build();
		}
	}

}