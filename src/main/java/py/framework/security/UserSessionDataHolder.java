package py.framework.security;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;

import lombok.AccessLevel;
import lombok.Setter;
import py.fiscalizaciones.domain.Usuario;
import py.fiscalizaciones.managers.UsuarioLogic;

@SessionScoped
public class UserSessionDataHolder implements Serializable {

	private static final long serialVersionUID = 9000522619431456058L;

	@Setter(AccessLevel.PROTECTED)
	Usuario user;

	@Inject
	UsuarioLogic usuarioLogic;

	public Usuario getUser() {
		if (user == null)
			user = usuarioLogic.getUsuarioByUsername("camilo.baez");
		return user;
	}

}