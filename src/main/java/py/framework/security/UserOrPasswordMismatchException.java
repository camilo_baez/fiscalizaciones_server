package py.framework.security;

public class UserOrPasswordMismatchException extends Exception {

	private static final long serialVersionUID = -2572540665994872791L;

	public UserOrPasswordMismatchException() {
		super("El usuario o la contraseña no coincide.");
	}

}
