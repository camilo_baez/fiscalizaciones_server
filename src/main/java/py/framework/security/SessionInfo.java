package py.framework.security;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.security.GeneralSecurityException;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import py.fiscalizaciones.domain.Usuario;
import py.fiscalizaciones.managers.UsuarioLogic;
import py.framework.webService.Ws_fiscalizaciones_portProxy;

@RequestScoped
public class SessionInfo implements Serializable {

	private static final long serialVersionUID = -6825391877753604019L;

	@Inject
	UsuarioLogic usuarioLogic;

	public Usuario authenticate(String username, String password)
			throws GeneralSecurityException, RemoteException, UserOrPasswordMismatchException {
		// Validamos que el usuario sea correcto
		// TODO: implementar autenticacion
		/*Ws_fiscalizaciones_portProxy wsProxy = new Ws_fiscalizaciones_portProxy();
		wsProxy.setEndpoint("https://www.mec.gov.py/gestion_intervenciones/ws_fiscalizaciones/action");
		if (wsProxy.loginws(username, password).equals("false"))
			throw new UserOrPasswordMismatchException();
		// Obtenemos los datos del usuario
		*/
		Usuario usuario = usuarioLogic.getUsuarioByUsername(username);
		return usuario;
	}

	public Usuario validate(String username, String password) throws RemoteException, UserOrPasswordMismatchException {
		// TODO: implementar validacion usuario
		/*Ws_fiscalizaciones_portProxy wsProxy = new Ws_fiscalizaciones_portProxy();
		wsProxy.setEndpoint("https://www.mec.gov.py/gestion_intervenciones/ws_fiscalizaciones/action");
		if (wsProxy.loginws(username, password).equals("false"))
			throw new UserOrPasswordMismatchException();
		else*/
			return usuarioLogic.getUsuarioByUsername(username);
	}
}
