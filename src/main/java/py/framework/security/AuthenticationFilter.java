package py.framework.security;

import java.io.IOException;

import javax.annotation.Priority;
import javax.inject.Inject;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.ext.Provider;

import com.google.common.collect.ImmutableMap;

import py.fiscalizaciones.domain.Usuario;

@Provider
@Priority(Priorities.AUTHENTICATION)
public class AuthenticationFilter implements ContainerRequestFilter {

	@Inject
	SessionInfo infoSession;

	@Inject
	UserSessionDataHolder dataHolder;

	@Override
	public void filter(ContainerRequestContext requestContext) throws IOException {
		UriInfo info = requestContext.getUriInfo();
		if (info.getPath().equals("/login"))
			return;
		try {
			String username = requestContext.getHeaderString("username");
			String password = requestContext.getHeaderString("password");
			Usuario user = infoSession.validate(username, password);
			dataHolder.setUser(user);
		} catch (Exception e) {
			requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED)
					.entity(ImmutableMap.of("error", "Fallo de validación del servicio.", "detail", e.getMessage()))
					.build());
		}
	}

}
