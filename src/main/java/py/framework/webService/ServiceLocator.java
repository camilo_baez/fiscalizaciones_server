/**
 * ServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package py.framework.webService;

public class ServiceLocator extends org.apache.axis.client.Service implements py.framework.webService.Service {

    public ServiceLocator() {
    }


    public ServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public ServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for ws_fiscalizaciones_port
    private java.lang.String ws_fiscalizaciones_port_address = "http://mec.gov.py/ws_fiscalizaciones/action";

    public java.lang.String getws_fiscalizaciones_portAddress() {
        return ws_fiscalizaciones_port_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String ws_fiscalizaciones_portWSDDServiceName = "ws_fiscalizaciones_port";

    public java.lang.String getws_fiscalizaciones_portWSDDServiceName() {
        return ws_fiscalizaciones_portWSDDServiceName;
    }

    public void setws_fiscalizaciones_portWSDDServiceName(java.lang.String name) {
        ws_fiscalizaciones_portWSDDServiceName = name;
    }

    public py.framework.webService.Ws_fiscalizaciones_port getws_fiscalizaciones_port() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(ws_fiscalizaciones_port_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getws_fiscalizaciones_port(endpoint);
    }

    public py.framework.webService.Ws_fiscalizaciones_port getws_fiscalizaciones_port(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            py.framework.webService.Ws_fiscalizaciones_bindingStub _stub = new py.framework.webService.Ws_fiscalizaciones_bindingStub(portAddress, this);
            _stub.setPortName(getws_fiscalizaciones_portWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setws_fiscalizaciones_portEndpointAddress(java.lang.String address) {
        ws_fiscalizaciones_port_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (py.framework.webService.Ws_fiscalizaciones_port.class.isAssignableFrom(serviceEndpointInterface)) {
                py.framework.webService.Ws_fiscalizaciones_bindingStub _stub = new py.framework.webService.Ws_fiscalizaciones_bindingStub(new java.net.URL(ws_fiscalizaciones_port_address), this);
                _stub.setPortName(getws_fiscalizaciones_portWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("ws_fiscalizaciones_port".equals(inputPortName)) {
            return getws_fiscalizaciones_port();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("gestion_intervenciones", "service");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("gestion_intervenciones", "ws_fiscalizaciones_port"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("ws_fiscalizaciones_port".equals(portName)) {
            setws_fiscalizaciones_portEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
