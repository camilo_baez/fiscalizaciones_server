/**
 * Ws_fiscalizaciones_port.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package py.framework.webService;

public interface Ws_fiscalizaciones_port extends java.rmi.Remote {
    public java.lang.String loginws(java.lang.String login, java.lang.String password) throws java.rmi.RemoteException;
}
