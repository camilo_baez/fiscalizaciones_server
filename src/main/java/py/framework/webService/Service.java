/**
 * Service.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package py.framework.webService;

public interface Service extends javax.xml.rpc.Service {
    public java.lang.String getws_fiscalizaciones_portAddress();

    public py.framework.webService.Ws_fiscalizaciones_port getws_fiscalizaciones_port() throws javax.xml.rpc.ServiceException;

    public py.framework.webService.Ws_fiscalizaciones_port getws_fiscalizaciones_port(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
