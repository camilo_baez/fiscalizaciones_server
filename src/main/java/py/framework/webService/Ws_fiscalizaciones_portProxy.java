package py.framework.webService;

public class Ws_fiscalizaciones_portProxy implements py.framework.webService.Ws_fiscalizaciones_port {
  private String _endpoint = null;
  private py.framework.webService.Ws_fiscalizaciones_port ws_fiscalizaciones_port = null;
  
  public Ws_fiscalizaciones_portProxy() {
    _initWs_fiscalizaciones_portProxy();
  }
  
  public Ws_fiscalizaciones_portProxy(String endpoint) {
    _endpoint = endpoint;
    _initWs_fiscalizaciones_portProxy();
  }
  
  private void _initWs_fiscalizaciones_portProxy() {
    try {
      ws_fiscalizaciones_port = (new py.framework.webService.ServiceLocator()).getws_fiscalizaciones_port();
      if (ws_fiscalizaciones_port != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)ws_fiscalizaciones_port)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)ws_fiscalizaciones_port)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (ws_fiscalizaciones_port != null)
      ((javax.xml.rpc.Stub)ws_fiscalizaciones_port)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public py.framework.webService.Ws_fiscalizaciones_port getWs_fiscalizaciones_port() {
    if (ws_fiscalizaciones_port == null)
      _initWs_fiscalizaciones_portProxy();
    return ws_fiscalizaciones_port;
  }
  
  public java.lang.String loginws(java.lang.String login, java.lang.String password) throws java.rmi.RemoteException{
    if (ws_fiscalizaciones_port == null)
      _initWs_fiscalizaciones_portProxy();
    return ws_fiscalizaciones_port.loginws(login, password);
  }
  
  
}