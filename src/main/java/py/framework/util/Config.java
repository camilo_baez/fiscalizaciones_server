package py.framework.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;

import org.apache.commons.lang3.Validate;

/**
 * Bean que facilita la confi a través de un archivo en el Sistema.
 *
 * @author sortiz
 *
 */
@ApplicationScoped
public class Config {

	private static String OS = System.getProperty("os.name").toLowerCase();

	public static boolean isWindows() {
		return (OS.indexOf("win") >= 0);
	}

	private static final String PROPERTY_FILE_LOCATION = "/";
	private static final String PROPERTY_FILE_NAME = "config.properties";

	private static final String UPLOAD_FOLDER_LINUX = "/opt/fiscalizaciones_upload/anexos/";
	private static final String SEPARADOR_LINUX = "/";

	private static final String UPLOAD_FOLDER_WIN = "C:\\fiscalizaciones_upload\\anexos\\";
	private static final String SEPARADOR_WIN = "\\";

	private Properties properties;

	@PostConstruct
	void readFile() {

		properties = new Properties();
		String configurationAbsolutePath = PROPERTY_FILE_LOCATION + PROPERTY_FILE_NAME;
		try (InputStream is = new FileInputStream(configurationAbsolutePath)) {
			properties.load(is);
		} catch (IOException fnfe) {
			System.out.println(
					"No se puede obtener archivo de configuración con path absoluto " + configurationAbsolutePath);
			System.out.println(
					"No se puede obtener archivo de configuración con path absoluto " + configurationAbsolutePath);
			System.out.println(
					"No se puede obtener archivo de configuración con path absoluto " + configurationAbsolutePath);
			System.out.println(
					"No se puede obtener archivo de configuración con path absoluto " + configurationAbsolutePath);

			System.out.println("Se busca en resources del war");
			ClassLoader classloader = Thread.currentThread().getContextClassLoader();
			try (InputStream stream = classloader.getResourceAsStream(PROPERTY_FILE_NAME)) {
				properties.load(stream);
			} catch (IOException e) {
				System.out.println(
						"No se puede obtener archivo de configuración desde resources, utilizando valores por defecto");
			}
		}

	}

	public boolean isDevel() {
		return getBool("debug.enabled", false);
	}

	public Integer getInteger(String key, Integer value) {
		return properties.getProperty(key) != null ? Integer.valueOf(properties.getProperty(key)) : value;
	}

	public String getString(String key, String de) {
		String toRet = properties.getProperty(Validate.notNull(key, "No se puede buscar con clave nula"));
		return toRet == null ? de : toRet;
	}

	public Boolean getBool(String key, Boolean def) {
		return properties.getProperty(key) != null ? Boolean.valueOf(properties.getProperty(key)) : def;
	}

	public int getSessionTimeout() {
		return getInteger("session.timeout", 50000);
	}

	public String getUploadFolder() {
		if (isWindows())
			return getString("server.uploadfolderwin", UPLOAD_FOLDER_WIN);
		return getString("server.uploadfolderlinux", UPLOAD_FOLDER_LINUX);
	}

	public String getSeparador() {
		if (isWindows())
			return getString("server.separadorwin", SEPARADOR_WIN);
		return getString("server.separadorlinux", SEPARADOR_LINUX);
	}

}
