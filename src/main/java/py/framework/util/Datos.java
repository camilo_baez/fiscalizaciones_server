package py.framework.util;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import py.fiscalizaciones.domain.SituacionesIntervencione;
import py.fiscalizaciones.domain.SolicitudesIntervencione;
import py.fiscalizaciones.domain.TiposVerificacionesFiscalizaciones;

@Data
public class Datos {
	private List<SolicitudesIntervencione> locales;

	private List<SituacionesIntervencione> situaciones;

	@JsonProperty("tipos_verificaciones")
	private List<TiposVerificacionesFiscalizaciones> verificaciones;

	public Datos() {
	}

}
