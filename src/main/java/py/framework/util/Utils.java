package py.framework.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.ws.rs.core.MultivaluedMap;

public class Utils {

	// Parse Content-Disposition header to get the original file name
	public static String parseFileName(MultivaluedMap<String, String> headers) {
		String[] contentDispositionHeader = headers.getFirst("Content-Disposition").split(";");
		for (String name : contentDispositionHeader) {
			if ((name.trim().startsWith("filename"))) {
				String[] tmp = name.split("=");
				String fileName = tmp[1].trim().replaceAll("\"", "");
				return fileName;
			}
		}
		return "randomName";
	}

	// Funcion que almacena el archivo en el filesystem
	public static Integer saveFile(InputStream uploadedInputStream, String serverLocation) {
		File file = new File(serverLocation);
		try {
			OutputStream outpuStream = new FileOutputStream(file);
			int read = 0;
			byte[] bytes = new byte[1024];
			while ((read = uploadedInputStream.read(bytes)) != -1) {
				outpuStream.write(bytes, 0, read);
			}
			outpuStream.flush();
			outpuStream.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return toIntExact(file.length());
	}

	// Funcion que valida la existencia de un directorio
	public static void verificarCarpeta(String carpeta) {
		File directory = new File(String.valueOf(carpeta));
		if (!directory.exists()) {
			directory.mkdirs();
			// If you require it to make the entire directory path including
			// parents,
			// use directory.mkdirs(); here instead.
		}
	}

	public static Integer toIntExact(long value) {
		if ((int) value != value) {
			throw new ArithmeticException("integer overflow");
		}
		return (int) value;
	}

}
