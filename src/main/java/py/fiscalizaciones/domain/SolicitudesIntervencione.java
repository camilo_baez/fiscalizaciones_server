package py.fiscalizaciones.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;


/**
 * The persistent class for the solicitudes_intervenciones database table.
 * 
 */
@Data
@Entity
@Table(name="solicitudes_intervenciones")
@NamedQuery(name="SolicitudesIntervencione.findAll", query="SELECT s FROM SolicitudesIntervencione s")
public class SolicitudesIntervencione implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	//bi-directional many-to-one association to Establecimiento
	@ManyToOne
	private Establecimiento establecimiento;

	//bi-directional many-to-one association to SolicitudesIntervencionesRequerimiento
	@OneToMany(mappedBy="solicitudesIntervencione")
	@LazyCollection(LazyCollectionOption.FALSE)
	@JsonProperty("obras")
	private List<SolicitudesIntervencionesRequerimiento> solicitudesIntervencionesRequerimientos;

	//bi-directional many-to-one association to SolicitudesIntervencionesRequerimiento
	@OneToMany(mappedBy="solicitudesIntervencione")
	@JsonIgnore
	private List<IntervencionesFiscalizadore> intervencionesFiscalizadore;
		
	public List<IntervencionesFiscalizadore> getIntervencionesFiscalizadore() {
		return intervencionesFiscalizadore;
	}

	public void setIntervencionesFiscalizadore(List<IntervencionesFiscalizadore> intervencionesFiscalizadore) {
		this.intervencionesFiscalizadore = intervencionesFiscalizadore;
	}

	public SolicitudesIntervencione() {
	}

}