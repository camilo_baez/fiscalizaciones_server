package py.fiscalizaciones.domain;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import lombok.Data;


/**
 * The persistent class for the situaciones_intervenciones database table.
 * 
 */
@Data
@Entity
@Table(name="situaciones_intervenciones")
@NamedQuery(name="SituacionesIntervencione.findAll", query="SELECT s FROM SituacionesIntervencione s")
public class SituacionesIntervencione implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	private String descripcion;

	public SituacionesIntervencione() {
	}

}