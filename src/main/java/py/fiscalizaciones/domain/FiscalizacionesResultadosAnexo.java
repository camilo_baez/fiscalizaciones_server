package py.fiscalizaciones.domain;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import lombok.Data;

/**
 * The persistent class for the fiscalizaciones_resultados_anexos database
 * table.
 * 
 */
@Data
@Entity
@Table(name = "fiscalizaciones_resultados_anexos")
@NamedQuery(name = "FiscalizacionesResultadosAnexo.findAll", query = "SELECT f FROM FiscalizacionesResultadosAnexo f")
@JsonInclude(Include.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class FiscalizacionesResultadosAnexo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@JsonProperty("id_anexo")
	private Integer id;

	@JsonIgnore
	@Column(name = "anexo_content_type")
	private String anexoContentType;

	@JsonIgnore
	@Column(name = "anexo_created_at")
	private Timestamp anexoCreatedAt;

	@Column(name = "anexo_file_name")
	private String anexoFileName;

	@JsonIgnore
	@Column(name = "anexo_file_size")
	private Integer anexoFileSize;

	@JsonIgnore
	@Column(name = "anexo_updated_at")
	private Timestamp anexoUpdatedAt;

	@JsonIgnore
	@Column(name = "created_at")
	private Timestamp createdAt;

	@JsonIgnore
	private String observacion;

	@JsonIgnore
	@Column(name = "updated_at")
	private Timestamp updatedAt;

	@JsonIgnore
	@Column(name = "usuario_registro_id")
	private Integer usuarioRegistroId;

	@JsonIgnore
	@Column(name = "tipo_adjunto_fiscalizacion_id")
	private Integer tipoAdjunto;

	@Transient
	@JsonSerialize
	private Integer id_logico;

	// bi-directional many-to-one association to FiscalizacionesResultado
	@ManyToOne
	@JoinColumn(name = "fiscalizacion_resultado_id")
	@JsonIgnore
	private FiscalizacionesResultado fiscalizacion;

}