package py.fiscalizaciones.domain;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

/**
 * The persistent class for the fiscalizaciones_observaciones database table.
 * 
 */
@Data
@Entity
@Table(name = "tipos_formularios_verificaciones")
@NamedQuery(name = "TiposFormulariosVerificaciones.findAll", query = "SELECT tfv FROM TiposFormulariosVerificaciones tfv")
public class TiposFormulariosVerificaciones implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@NotNull
	@JsonIgnore
	private String descripcion;

	@Column(name = "created_at", updatable = false)
	@JsonIgnore
	private Timestamp createdAt;

	@Column(name = "updated_at")
	@JsonIgnore
	private Timestamp updatedAt;

}