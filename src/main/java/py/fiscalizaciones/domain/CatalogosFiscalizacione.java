package py.fiscalizaciones.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;


/**
 * The persistent class for the catalogos_fiscalizaciones database table.
 * 
 */
@Data
@Entity
@Table(name="catalogos_fiscalizaciones")
@NamedQuery(name="CatalogosFiscalizacione.findAll", query="SELECT c FROM CatalogosFiscalizacione c")
public class CatalogosFiscalizacione implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@JsonIgnore
	private Integer id;

	private String descripcion;

	//bi-directional many-to-one association to SolicitudesIntervencionesRequerimiento
	@OneToMany(mappedBy="catalogosFiscalizacione")
	@JsonIgnore
	private List<SolicitudesIntervencionesRequerimiento> solicitudesIntervencionesRequerimientos;

	public CatalogosFiscalizacione() {
	}
	
}