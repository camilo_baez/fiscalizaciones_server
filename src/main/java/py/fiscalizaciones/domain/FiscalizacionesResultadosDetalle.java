package py.fiscalizaciones.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

/**
 * The persistent class for the fiscalizaciones_resultados_detalles database
 * table.
 * 
 */
@Data
@Entity
@Table(name = "fiscalizaciones_resultados_detalles")
@NamedQuery(name = "FiscalizacionesResultadosDetalle.findAll", query = "SELECT f FROM FiscalizacionesResultadosDetalle f")
@JsonInclude(Include.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class FiscalizacionesResultadosDetalle implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@JsonProperty("id_avance_det")
	private Integer id;

	@Column(name = "origen_app")
	private Boolean origenApp;

	@JsonProperty("id_item")
	@Column(name = "catalogo_requerimiento_rubro_id")
	private Integer catalogoRequerimientoRubroId;

	@Column(name = "created_at", updatable = false)
	private Timestamp createdAt;

	@Column(name = "factor_intervencion")
	@JsonProperty("factor_intervencion")
	private BigDecimal factorIntervencion;

	@Temporal(TemporalType.DATE)
	@Column(name = "fecha_supervision")
	private Date fechaSupervision;

	private String observacion;

	@Column(name = "observacion_supervisor")
	@JsonProperty("observacion_supervisor")
	private String observacionSupervisor;

	@JsonProperty("id_situacion")
	@Column(name = "situacion_intervencion_id")
	private Integer situacionIntervencionId;

	@Column(name = "updated_at")
	private Timestamp updatedAt;

	@Column(name = "usuario_registro_id", updatable = false)
	private Integer usuarioRegistroId;

	@Column(name = "usuario_supervisor_id")
	private Integer usuarioSupervisorId;

	// bi-directional many-to-one association to FiscalizacionesResultado
	@ManyToOne
	@JoinColumn(name = "fiscalizacion_resultado_id")
	@JsonIgnore
	private FiscalizacionesResultado fiscalizacion;

}