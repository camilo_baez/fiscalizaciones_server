package py.fiscalizaciones.domain;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;


/**
 * The persistent class for the catalogos_requerimientos_rubros database table.
 * 
 */
@Data
@Entity
@Table(name="catalogos_requerimientos_rubros")
@NamedQuery(name="CatalogosRequerimientosRubro.findAll", query="SELECT c FROM CatalogosRequerimientosRubro c")
public class CatalogosRequerimientosRubro implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@JsonProperty("id_item")
	private Integer id;
	
	private BigDecimal cantidad;

	//bi-directional many-to-one association to Rubro
	@ManyToOne
	@JsonProperty("item")
	private Rubro rubro;

	//bi-directional many-to-one association to SolicitudesIntervencionesRequerimiento
	@ManyToOne
	@JoinColumn(name="solicitud_intervencion_requerimiento_id")
	@JsonIgnore
	private SolicitudesIntervencionesRequerimiento solicitudesIntervencionesRequerimiento;

	public CatalogosRequerimientosRubro() {
	}

}