package py.fiscalizaciones.domain;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

/**
 * The persistent class for the fiscalizaciones_observaciones database table.
 * 
 */
@Data
@Entity
@Table(name = "verificaciones_fiscalizaciones")
@NamedQuery(name = "VerificacionesFiscalizaciones.findAll", query = "SELECT vf FROM VerificacionesFiscalizaciones vf")
public class VerificacionesFiscalizaciones implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@NotNull
	private String descripcion;

	@NotNull
	private Boolean activo;

	@Column(name = "created_at", updatable = false)
	@JsonIgnore
	private Timestamp createdAt;

	@Column(name = "updated_at")
	@JsonIgnore
	private Timestamp updatedAt;

	@ManyToOne
	@JoinColumn(name = "tipo_verificacion_id")
	@JsonIgnore
	private TiposVerificacionesFiscalizaciones tipoVerificacion;

}