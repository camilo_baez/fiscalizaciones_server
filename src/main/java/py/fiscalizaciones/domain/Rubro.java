package py.fiscalizaciones.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * The persistent class for the rubros database table.
 * 
 */
@Entity
@Table(name="rubros")
@NamedQuery(name="Rubro.findAll", query="SELECT r FROM Rubro r")
public class Rubro implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@JsonIgnore
	private Integer id;

	private String descripcion;

	@ManyToOne
	@JoinColumn(name="unidad_medida_id")
	@JsonProperty("unidad_medida")
	private UnidadesMedida unidadMedida;

	//bi-directional many-to-one association to CatalogosRequerimientosRubro
	@OneToMany(mappedBy="rubro")
	@JsonIgnore
	private List<CatalogosRequerimientosRubro> catalogosRequerimientosRubros;

	public Rubro() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public UnidadesMedida geUnidadMedida() {
		return this.unidadMedida;
	}

	public void setUnidadMedida(UnidadesMedida unidadMedida) {
		this.unidadMedida = unidadMedida;
	}

	public List<CatalogosRequerimientosRubro> getCatalogosRequerimientosRubros() {
		return this.catalogosRequerimientosRubros;
	}

	public void setCatalogosRequerimientosRubros(List<CatalogosRequerimientosRubro> catalogosRequerimientosRubros) {
		this.catalogosRequerimientosRubros = catalogosRequerimientosRubros;
	}

	public CatalogosRequerimientosRubro addCatalogosRequerimientosRubro(CatalogosRequerimientosRubro catalogosRequerimientosRubro) {
		getCatalogosRequerimientosRubros().add(catalogosRequerimientosRubro);
		catalogosRequerimientosRubro.setRubro(this);

		return catalogosRequerimientosRubro;
	}

	public CatalogosRequerimientosRubro removeCatalogosRequerimientosRubro(CatalogosRequerimientosRubro catalogosRequerimientosRubro) {
		getCatalogosRequerimientosRubros().remove(catalogosRequerimientosRubro);
		catalogosRequerimientosRubro.setRubro(null);

		return catalogosRequerimientosRubro;
	}

}