package py.fiscalizaciones.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;


/**
 * The persistent class for the solicitudes_intervenciones_requerimientos database table.
 * 
 */
@Data
@Entity
@Table(name="solicitudes_intervenciones_requerimientos")
@NamedQuery(name="SolicitudesIntervencionesRequerimiento.findAll", query="SELECT s FROM SolicitudesIntervencionesRequerimiento s")
public class SolicitudesIntervencionesRequerimiento implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@JsonProperty("id_obra")
	private Integer id;

	@Column(name="estado_obra_id")
	//@JsonIgnore
	private Integer estadoObraId;

	//bi-directional many-to-one association to CatalogosRequerimientosRubro
	@OneToMany(mappedBy="solicitudesIntervencionesRequerimiento", fetch = FetchType.EAGER, cascade = CascadeType.MERGE, orphanRemoval = true)
	@JsonProperty("items")
	@Fetch(value = FetchMode.SUBSELECT)
	private List<CatalogosRequerimientosRubro> catalogosRequerimientosRubros;

	//bi-directional many-to-one association to CatalogosFiscalizacione
	@ManyToOne
	@JoinColumn(name="catalogo_fiscalizacion_id")
	@JsonProperty("obra")
	private CatalogosFiscalizacione catalogosFiscalizacione;

	//bi-directional many-to-one association to SolicitudesIntervencione
	@ManyToOne
	@JoinColumn(name="solicitud_intervencion_id")
	@JsonIgnore
	private SolicitudesIntervencione solicitudesIntervencione;
	

	@OneToMany(mappedBy="solicitudesIntervencionesRequerimiento", fetch = FetchType.EAGER, cascade = CascadeType.MERGE, orphanRemoval = true)
	@JsonProperty("fiscalizaciones")
	@Fetch(value = FetchMode.SUBSELECT)
	private List<FiscalizacionesResultado> fiscalizacionesResultado;

	public SolicitudesIntervencionesRequerimiento() {
	}

}