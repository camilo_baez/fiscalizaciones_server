package py.fiscalizaciones.domain;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

/**
 * The persistent class for the fiscalizaciones_observaciones database table.
 * 
 */
@Data
@Entity
@Table(name = "fiscalizaciones_observaciones")
@NamedQuery(name = "FiscalizacionesObservaciones.findAll", query = "SELECT fo FROM FiscalizacionesObservaciones fo")
public class FiscalizacionesObservaciones implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@NotNull
	private String observacion;

	@NotNull
	@Column(name = "medida_adoptada")
	private String medidaAdoptada;

	@Column(name = "created_at", updatable = false)
	private Timestamp createdAt;

	@Column(name = "updated_at")
	private Timestamp updatedAt;

	// bi-directional many-to-one association to FiscalizacionesResultado
	@ManyToOne
	@JoinColumn(name = "fiscalizacion_resultado_id")
	@JsonIgnore
	private FiscalizacionesResultado fiscalizacion;

}