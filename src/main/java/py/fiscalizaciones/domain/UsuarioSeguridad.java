package py.fiscalizaciones.domain;

import javax.persistence.Cacheable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Entity
@Data
@Cacheable
@Table(name = "sec_usuarios", schema = "public")
public class UsuarioSeguridad {

	@Id
	private Integer id;

	@NotNull
	private String login;

	@NotNull
	@OneToOne
	@JoinColumn(name = "persona_id")
	private Usuario usuario;

}
