package py.fiscalizaciones.domain;

import java.util.List;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Entity
@Data
@Cacheable
@Table(name = "personas", schema = "public")
public class Usuario {

	@Id
	@Column(name = "id_persona")
	private long id;

	@NotNull
	@Column(name = "nombre_persona")
	private String nombrePersona;

	@Column(name = "documento_persona")
	private String documento;

	@Column(name = "ruc_persona")
	private String ruc;

	@NotNull
	@OneToOne(mappedBy = "usuario")
	@JsonIgnore
	private UsuarioSeguridad usuarioSeguridad;
	
	//bi-directional many-to-one association to SolicitudesIntervencionesRequerimiento
	@OneToMany(mappedBy="persona")
	private List<IntervencionesFiscalizadore> intervencionesFiscalizadore;

}
