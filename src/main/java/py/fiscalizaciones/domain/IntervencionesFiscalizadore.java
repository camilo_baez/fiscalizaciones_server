package py.fiscalizaciones.domain;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Data;


/**
 * The persistent class for the intervenciones_fiscalizadores database table.
 * 
 */
@Data
@Entity
@Table(name="intervenciones_fiscalizadores")
@NamedQuery(name="IntervencionesFiscalizadore.findAll", query="SELECT i FROM IntervencionesFiscalizadore i")
public class IntervencionesFiscalizadore implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	@Column(name="created_at")
	private Timestamp createdAt;

	@Temporal(TemporalType.DATE)
	@Column(name="fecha_prevista_respuesta")
	private Date fechaPrevistaRespuesta;

	private Boolean resolucion;

	@Column(name="updated_at")
	private Timestamp updatedAt;

	//bi-directional many-to-one association to Persona
	@ManyToOne
	@JoinColumn(name="persona_id")
	private Usuario persona;

	//bi-directional many-to-one association to SolicitudesIntervencione
	@ManyToOne
	@JoinColumn(name="solicitud_intervencion_id")
	private SolicitudesIntervencione solicitudesIntervencione;

	public IntervencionesFiscalizadore() {
	}

}