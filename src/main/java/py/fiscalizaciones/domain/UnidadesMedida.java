package py.fiscalizaciones.domain;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * The persistent class for the unidades_medidas database table.
 * 
 */
@Entity
@Table(name="unidades_medidas")
@NamedQuery(name="UnidadesMedida.findAll", query="SELECT u FROM UnidadesMedida u")
public class UnidadesMedida implements Serializable {
	private static final long serialVersionUID = 1L;

	@JsonProperty("descripcion")
	private String abreviatura;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@JsonIgnore
	private Integer id;

	public UnidadesMedida() {
	}

	public String getAbreviatura() {
		return this.abreviatura;
	}

	public void setAbreviatura(String abreviatura) {
		this.abreviatura = abreviatura;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
}