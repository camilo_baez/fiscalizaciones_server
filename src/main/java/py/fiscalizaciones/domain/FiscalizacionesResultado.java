package py.fiscalizaciones.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Where;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import lombok.Data;

/**
 * The persistent class for the fiscalizaciones_resultados database table.
 * 
 */
@Data
@Entity
@Table(name = "fiscalizaciones_resultados")
@NamedQuery(name = "FiscalizacionesResultado.findAll", query = "SELECT f FROM FiscalizacionesResultado f")
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(Include.NON_EMPTY)
public class FiscalizacionesResultado implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@JsonProperty("id_avance")
	private Integer id;

	@Column(name = "avance_intervencion")
	private BigDecimal avanceIntervencion;

	private String conclusion;

	private String control;

	@Column(name = "origen_app")
	@JsonProperty("origen_app")
	private Boolean origenApp;

	@Column(name = "created_at", updatable = false)
	private Timestamp createdAt;

	@NotNull
	@Temporal(TemporalType.DATE)
	@Column(name = "fecha_intervencion")
	@JsonProperty("fecha_intervencion")
	@JsonFormat(shape = JsonFormat.Shape.NUMBER)
	private Date fechaIntervencion;

	@Column(name = "numero_certificacion")
	@JsonProperty("numero_certificado")
	private Integer numeroCertificacion;

	private String observacion;

	@Column(name = "orden_trabajo")
	private String ordenTrabajo;

	@NotNull
	@Column(name = "solicitud_intervencion_requerimiento_id")
	// @JsonIgnore
	@JsonProperty("id_obra")
	private Integer solicitudIntervencionRequerimientoId;

	// bi-directional many-to-one association to SolicitudesIntervencione
	@ManyToOne
	@JoinColumn(name = "solicitud_intervencion_requerimiento_id", insertable = false, updatable = false)
	// @JsonProperty("id_obra")
	@JsonIgnore
	private SolicitudesIntervencionesRequerimiento solicitudesIntervencionesRequerimiento;

	@Column(name = "updated_at")
	private Timestamp updatedAt;

	@Column(name = "usuario_registro_id", updatable = false)
	private Integer usuarioRegistroId;

	private String longitud;

	private String latitud;

	@Transient
	@JsonSerialize
	private Integer id_logico;

	@OneToMany(mappedBy = "fiscalizacion", fetch = FetchType.EAGER, cascade = CascadeType.MERGE, orphanRemoval = true)
	@Fetch(value = FetchMode.SUBSELECT)
	private List<FiscalizacionesResultadosDetalle> avances;

	@OneToMany(mappedBy = "fiscalizacion", fetch = FetchType.EAGER, cascade = CascadeType.MERGE, orphanRemoval = true)
	@Fetch(value = FetchMode.SUBSELECT)
	@Where(clause = "tipo_adjunto_fiscalizacion_id IN (8,9)")
	private List<FiscalizacionesResultadosAnexo> anexos;

	@OneToMany(mappedBy = "fiscalizacion", fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
	@Fetch(value = FetchMode.SUBSELECT)
	private List<FiscalizacionesResultadosVerificaciones> verificaciones;

	@OneToMany(mappedBy = "fiscalizacion", fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
	@Fetch(value = FetchMode.SUBSELECT)
	private List<FiscalizacionesObservaciones> observaciones;

}