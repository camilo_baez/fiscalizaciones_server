package py.fiscalizaciones.domain;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

/**
 * The persistent class for the fiscalizaciones_observaciones database table.
 * 
 */
@Data
@Entity
@Table(name = "tipos_verificaciones_fiscalizaciones")
@NamedQuery(name = "TiposVerificacionesFiscalizaciones.findAll", query = "SELECT tvf FROM TiposVerificacionesFiscalizaciones tvf")
public class TiposVerificacionesFiscalizaciones implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@JsonProperty("id_tipo")
	private Integer id;

	@NotNull
	private String descripcion;

	@Column(name = "dato_unico")
	@JsonIgnore
	private Boolean datoUnico;

	@Column(name = "created_at", updatable = false)
	@JsonIgnore
	private Timestamp createdAt;

	@Column(name = "updated_at")
	@JsonIgnore
	private Timestamp updatedAt;

	@ManyToOne
	@JoinColumn(name = "tipo_formulario_verificacion_id")
	private TiposFormulariosVerificaciones tipo_formulario;

	@OneToMany(mappedBy = "tipoVerificacion", fetch = FetchType.EAGER)
	private List<VerificacionesFiscalizaciones> opciones;

}