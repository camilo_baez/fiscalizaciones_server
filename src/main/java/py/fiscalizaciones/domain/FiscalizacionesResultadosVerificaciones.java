package py.fiscalizaciones.domain;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

/**
 * The persistent class for the fiscalizaciones_observaciones database table.
 * 
 */
@Data
@Entity
@Table(name = "fiscalizaciones_resultados_verificaciones")
@NamedQuery(name = "FiscalizacionesResultadosVerificaciones.findAll", query = "SELECT frv FROM FiscalizacionesResultadosVerificaciones frv")
public class FiscalizacionesResultadosVerificaciones implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	private Boolean cumplimiento;

	@Column(name = "valor_descriptivo")
	private String valorDescriptivo;

	@Column(name = "valor_numerico")
	private Integer valorNumerico;

	@Column(name = "created_at", updatable = false)
	@JsonIgnore
	private Timestamp createdAt;

	@Column(name = "updated_at")
	@JsonIgnore
	private Timestamp updatedAt;

	// bi-directional many-to-one association to FiscalizacionesResultado
	@ManyToOne
	@JoinColumn(name = "fiscalizacion_resultado_id")
	@JsonIgnore
	private FiscalizacionesResultado fiscalizacion;

	@ManyToOne
	@JoinColumn(name = "verificacion_fiscalizacion_id")
	private VerificacionesFiscalizaciones verificacion;

}