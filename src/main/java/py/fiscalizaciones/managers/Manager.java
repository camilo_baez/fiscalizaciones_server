package py.fiscalizaciones.managers;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.Collectors;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.lang3.Validate;
import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;
import org.json.JSONArray;
import org.json.JSONObject;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableMap;

import py.fiscalizaciones.domain.FiscalizacionesObservaciones;
import py.fiscalizaciones.domain.FiscalizacionesResultado;
import py.fiscalizaciones.domain.FiscalizacionesResultadosDetalle;
import py.fiscalizaciones.domain.FiscalizacionesResultadosVerificaciones;
import py.fiscalizaciones.domain.SituacionesIntervencione;
import py.fiscalizaciones.domain.SolicitudesIntervencione;
import py.fiscalizaciones.domain.SolicitudesIntervencionesRequerimiento;
import py.fiscalizaciones.domain.TiposVerificacionesFiscalizaciones;
import py.framework.security.UserSessionDataHolder;
import py.framework.util.Datos;

@Stateless
@LocalBean
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class Manager {

	@Inject
	UserSessionDataHolder dataHolder;

	@Inject
	AnexoLogic anexoLogic;

	// para las respuestas
	JSONObject resp;
	@PersistenceContext(unitName = "fiscalizacion")
	private EntityManager em;

	// @formatter:off
	/**
	 * Retorna el ultimo número de certificación generado
	 */
	public static final String QUERY_MAX_CERTIFICADO_NUMERO = ""
			+ " SELECT coalesce(MAX(fr.numeroCertificacion), 0) + 1 " + " FROM FiscalizacionesResultado fr "
			+ " WHERE fr.solicitudIntervencionRequerimientoId=:idObra ";

	/**
	 * Retorna el ultimo número de consulta generado
	 */
	public static final String QUERY_MAX_CERTIFICADO_FECHA = ""
			+ " SELECT coalesce(MAX(fr.fechaIntervencion), '03/03/1973') " + " FROM FiscalizacionesResultado fr "
			+ " WHERE fr.solicitudIntervencionRequerimientoId=:idObra " + " AND fr.numeroCertificacion IS NOT NULL ";

	/**
	 * Retorna si una fiscalizacion existe en fecha
	 */
	public static final String QUERY_EXISTE_FISCALIZACION = "" + " SELECT fr " + " FROM FiscalizacionesResultado fr "
			+ " WHERE fr.solicitudIntervencionRequerimientoId=:idObra "
			+ " AND fr.fechaIntervencion=:fechaIntervencion";

	/**
	 * Retorna el listado de fiscalizaciones que pertenecen a una obra, pero que
	 * no seran actualizadas
	 */
	public static final String QUERY_OBTENER_FISCALIZACIONES = "" + " SELECT fr " + " FROM FiscalizacionesResultado fr "
			+ " WHERE fr.solicitudIntervencionRequerimientoId IN (:idObras) "
			+ " AND fr.id NOT IN (:idFiscalizaciones) ";

	/**
	 * Retorna el listado de locales de un fiscalizador
	 */
	public static final String QUERY_OBTENER_SOLICITUDES_BY_FISCALIZADOR = "" + " SELECT distinct si "
			+ " FROM SolicitudesIntervencione si " + " JOIN si.intervencionesFiscalizadore inter "
			+ " inner join fetch si.solicitudesIntervencionesRequerimientos as req"
			+ " inner join req.catalogosRequerimientosRubros as item" + " WHERE inter.persona.id = :idFiscalizador"
			+ " and req.estadoObraId > 1"
			+ " ORDER BY si.id, req.id";
	// @formatter:on

	String usuario;
	String contrasena;
	int idFiscalizador;
	int idUsuario = 1;

	/**
	 * Autenticación de usuario
	 * 
	 * @param usuario
	 * @param contrasena
	 * @return JSONObject con el status, mensaje y en caso de éxito el listado
	 *         de establecimientos y situaciones
	 */
	public String login(String usuario, String contrasena) {
		this.usuario = usuario;
		this.contrasena = contrasena;

		resp = new JSONObject();
		if (usuario == null || contrasena == null) {
			resp.put("status", "1");
			resp.put("mensaje", "Error al autenticar. No se admiten parametros nulos");
			resp.put("objeto", "{}");
		} else {
			try {
				idFiscalizador = getIdFiscalizador();
				if (idFiscalizador > 0) {
					resp.put("status", "0");
					resp.put("mensaje", "Autenticado correctamente");
					resp.put("objeto", obtenerDatosAnterior());
				} else {
					resp.put("status", "2");
					resp.put("mensaje", "Datos incorrectos");
					resp.put("objeto", "{}");
				}
			} catch (Exception exc) {
				exc.printStackTrace();
				resp.put("status", "3");
				resp.put("mensaje", "Error al autenticar: " + exc.getMessage());
				resp.put("objeto", "{}");
			}
		}
		return resp.toString();
	}

	/**
	 * Obtiene el listado completo de los establecimientos junto con sus
	 * requerimientos y rubros, asi como tambien los avances para cada
	 * requerimiento
	 * 
	 * @return Objeto con el listado de establecimientos y situaciones
	 */
	public Response obtenerDatosAnterior() {
		// Obtiene la lista de locales, con sus obras y detalles
		JSONArray locales = getLocales();
		JSONArray avances = getAvances();
		JSONArray anexos = getAnexos();
		JSONArray situaciones = getSituaciones();

		asignarAnexoAAvance(avances, anexos);
		asignarAvanceAObra(avances, locales);

		JSONObject objeto = new JSONObject();
		objeto.put("locales", locales);
		objeto.put("situaciones", situaciones);

		return Response.ok().entity(objeto.toString()).build();
	}

	public Response obtenerDatos() {
		// Obtiene la lista de locales, con sus obras y detalles
		List<SolicitudesIntervencione> listadoSolicitudes = em
				.createQuery(QUERY_OBTENER_SOLICITUDES_BY_FISCALIZADOR, SolicitudesIntervencione.class)
				.setParameter("idFiscalizador", dataHolder.getUser().getId()).getResultList();
		for (SolicitudesIntervencione si : listadoSolicitudes) {
			for (SolicitudesIntervencionesRequerimiento sir : si.getSolicitudesIntervencionesRequerimientos()) {
				sir.getFiscalizacionesResultado()
						.sort(Comparator.comparing(FiscalizacionesResultado::getFechaIntervencion));
			}
		}
		List<SituacionesIntervencione> situaciones = em
				.createNamedQuery("SituacionesIntervencione.findAll", SituacionesIntervencione.class).getResultList();
		List<TiposVerificacionesFiscalizaciones> verificaciones = em.createNamedQuery(
				"TiposVerificacionesFiscalizaciones.findAll", TiposVerificacionesFiscalizaciones.class).getResultList();
		Datos datos = new Datos();
		datos.setLocales(listadoSolicitudes);
		datos.setSituaciones(situaciones);
		datos.setVerificaciones(verificaciones);
		return Response.ok().entity(datos).build();
	}

	/**
	 * A partir del listado de establecimientos con sus requerimientos y rubros
	 * asigna los avances realizados a cada requerimiento
	 * 
	 * @param avances
	 * @param locales
	 */
	public void asignarAvanceAObra(JSONArray avances, JSONArray locales) {
		// Por cada obra asignar el listado de fiscalizaciones correspondiente
		for (int i = 0; i < locales.length(); i++) {
			JSONObject local = locales.getJSONObject(i);
			JSONArray obras = local.getJSONArray("obras");
			for (int j = 0; j < obras.length(); j++) {
				JSONObject obra = obras.getJSONObject(j);
				JSONArray avancesList = new JSONArray();
				for (int k = 0; k < avances.length(); k++) {
					JSONObject avance = avances.getJSONObject(k);
					if (obra.get("id_obra") == avance.get("id_obra")) {
						avancesList.put(avance);
					}
				}
				if (avancesList.length() > 0) {
					obra.put("fiscalizaciones", avancesList);
				}
			}
		}
	}

	/**
	 * A partir del listado de establecimientos con sus requerimientos y rubros
	 * asigna los avances realizados a cada requerimiento
	 * 
	 * @param avances
	 * @param locales
	 */
	public void asignarAnexoAAvance(JSONArray avances, JSONArray anexos) {
		// Por cada avance asignar el listado de anexos correspondiente
		JSONArray anexosList;
		for (int k = 0; k < avances.length(); k++) {
			JSONObject avance = avances.getJSONObject(k);
			anexosList = new JSONArray();
			for (int i = 0; i < anexos.length(); i++) {
				JSONObject anexo = anexos.getJSONObject(i);
				if (avance.getInt("id_avance") == anexo.getInt("id_avance")) {
					anexosList.put(anexo);
				}
			}
			if (anexosList.length() > 0) {
				avance.put("anexos", anexosList);
			}
		}
	}

	/**
	 * A partir del id de fiscalizador obtiene el listado de establecimientos
	 * con sus requerimientos y rubros
	 * 
	 * @return JSONArray de establecimientos
	 */
	public JSONArray getLocales() {
		JSONObject local = null;
		JSONObject obra = null;
		JSONObject item = null;
		JSONArray locales = new JSONArray();
		JSONArray obras = new JSONArray();
		JSONArray items = new JSONArray();

		List<Object[]> resultadoList = getLocalesList();
		// Procesar de manera lineal e insertar como objetos JSON
		// El listado debe estar ordenado segun: establecimiento,
		// requerimientos, rubros
		for (Object[] resultado : resultadoList) {

			if (local != null) {/// Se actualiza el objeto solo si es nuevo
				if (local.getInt("id_establecimiento") != ((Integer) resultado[0]).intValue()) {
					items.put(item);
					obra.put("items", items);
					obras.put(obra);
					local.put("obras", obras);
					locales.put(local);
					local = inicializarLocal(resultado);
					obra = null;
					obras = new JSONArray();
				}
			} else {
				local = inicializarLocal(resultado);
			}
			if (obra != null) {/// Se actualiza el objeto solo si es nuevo
				if (obra.getInt("id_obra") != ((Integer) resultado[2]).intValue()) {
					items.put(item);
					obra.put("items", items);
					obras.put(obra);
					obra = inicializarObra(resultado);
					item = null;
					items = new JSONArray();
				}
			} else {
				obra = inicializarObra(resultado);
			}
			if (item != null) {/// Se actualiza el objeto solo si es nuevo
				if (item.getInt("id_item") != ((Integer) resultado[5]).intValue()) {
					items.put(item);
					item = inicializarItem(resultado);
				}
			} else {
				item = inicializarItem(resultado);
			}
		}

		if (!resultadoList.isEmpty()) {
			items.put(item);
			obra.put("items", items);
			obras.put(obra);
			local.put("obras", obras);
			locales.put(local);
		}
		return locales;
	}

	/**
	 * A partir del id de fiscalizador obtiene los avances realizados sobre los
	 * requerimientos de los establecimientos asignados
	 * 
	 * @return JSONArray de los avances con sus detalles
	 */
	public JSONArray getAvances() {
		JSONObject avance = null;
		JSONObject avanceDet = null;
		JSONArray avances = new JSONArray();
		JSONArray avanceDets = new JSONArray();

		List<Object[]> resultadoAvancesList = getAvancesList();
		for (Object[] resultado : resultadoAvancesList) {

			if (avance != null) {/// Se actualiza el objeto solo si es nuevo
				if (avance.get("id_avance") != resultado[0]) {
					avanceDets.put(avanceDet);
					avance.put("avances", avanceDets);
					avances.put(avance);
					avance = inicializarAvance(resultado);
					avanceDets = new JSONArray();
					avanceDet = null;
				}
			} else {
				avance = inicializarAvance(resultado);
			}
			if (avanceDet != null) {/// Se actualiza el objeto solo si es nuevo
				if (avanceDet.get("id_avance_det") != resultado[3]) {
					avanceDets.put(avanceDet);
					avanceDet = inicializarAvanceDet(resultado);
				}
			} else {
				avanceDet = inicializarAvanceDet(resultado);
			}

		}

		if (!resultadoAvancesList.isEmpty()) {
			avanceDets.put(avanceDet);
			avance.put("avances", avanceDets);
			avances.put(avance);
		}

		return avances;
	}

	/**
	 * A partir del id de fiscalizador obtiene los anexos realizados sobre las
	 * fiscalizaciones de los requerimientos de los establecimientos asignados
	 * 
	 * @return JSONArray de los anexos de una fiscalizacion
	 */
	public JSONArray getAnexos() {
		JSONObject anexo = null;
		JSONArray anexos = new JSONArray();

		List<Object[]> resultadoAnexosList = getAnexosList();
		for (Object[] resultado : resultadoAnexosList) {
			anexo = inicializarAnexo(resultado);
			anexos.put(anexo);
		}

		return anexos;
	}

	/**
	 * Obtiene un listado de las situaciones_intervencion para las opciones en
	 * el LOV
	 * 
	 * @return JSONArray de situaciones
	 */
	public JSONArray getSituaciones() {
		JSONObject situacion = null;
		JSONArray situaciones = new JSONArray();

		List<Object[]> resultadoList = getSituacionesList();
		for (Object[] resultado : resultadoList) {
			situacion = new JSONObject();
			situacion.put("id", resultado[0]);
			situacion.put("descripcion", resultado[1]);

			situaciones.put(situacion);
		}
		return situaciones;
	}

	/**
	 * Consulta para login
	 * 
	 * @return query
	 */
	public int getIdFiscalizador() {
		Query query = em.createNativeQuery("select persona_id from sec_usuarios " + "where login = :usuario "
				+ "and crypted_password = :contrasena and active is true");

		query.setParameter("usuario", this.usuario);
		query.setParameter("contrasena", this.contrasena);
		int idFiscalizador = 0;
		try {
			idFiscalizador = ((Integer) query.getSingleResult()).intValue();

		} catch (Exception ex) {
			return 0;
		}

		return idFiscalizador;
	}

	/**
	 * Listado de locales sin procesar
	 * 
	 * @return listado de locales
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> getLocalesList() {
		Query query = em.createNativeQuery("select sol.id as id_establecimiento, "
				+ "est.nombre_establecimiento as establecimiento, " + "sol_req.id as id_obra, cat.descripcion as obra, "
				+ "sol_req.estado_obra_id as id_estado, " + "sol_req_det.id as id_item, rub.descripcion as item, "
				+ "uni.abreviatura as unidad_medida, " + "sol_req_det.cantidad as cantidad "
				+ "from solicitudes_intervenciones sol " + "join solicitudes_intervenciones_requerimientos sol_req "
				+ "on (sol_req.solicitud_intervencion_id = sol.id) "
				+ "join catalogos_requerimientos_rubros sol_req_det "
				+ "on (sol_req_det.solicitud_intervencion_requerimiento_id = sol_req.id) "
				+ "join establecimientos est on (est.id = sol.establecimiento_id) "
				+ "join catalogos_fiscalizaciones cat " + "on (cat.id = sol_req.catalogo_fiscalizacion_id) "
				+ "join rubros rub on (rub.id = sol_req_det.rubro_id) "
				+ "join unidades_medidas uni on (uni.id = rub.unidad_medida_id) "
				+ "join intervenciones_fiscalizadores i on sol.id = i.solicitud_intervencion_id "
				+ "where i.persona_id = :id_fiscalizador and sol_req.estado_obra_id in (2, 3, 4) "
				+ "order by sol.establecimiento_id, sol_req.id, rub.id");
		query.setParameter("id_fiscalizador", dataHolder.getUser().getId());

		List<Object[]> resultadoList = new ArrayList<Object[]>();
		try {
			resultadoList = query.getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return resultadoList;
	}

	/**
	 * Listado de avances sin procesar
	 * 
	 * @return listado de avances
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> getAvancesList() {
		Query query = em.createNativeQuery("select res.id as id_avance, " + "sol.id as solicitud_intervencion_id, "
				+ "(EXTRACT(EPOCH FROM res.fecha_intervencion AT TIME ZONE 'UTC') * 1000) as fecha_intervencion, "
				+ "res.longitud as longitud, " + "res.latitud as latitud, " + "sol_req.id as id_obra, res.origen_app, "
				+ "res.numero_certificacion, "
				+ "res_det.id id_avance_det, coalesce(res_det.factor_intervencion,0) as factor_intervencion, "
				+ "res_det.observacion as observacion, " + "res_det.observacion_supervisor as observacion_supervisor, "
				+ "res_det.catalogo_requerimiento_rubro_id as id_item, " + "sit.id "
				+ "from solicitudes_intervenciones sol " + "join solicitudes_intervenciones_requerimientos sol_req "
				+ "on (sol_req.solicitud_intervencion_id = sol.id) "
				+ "join catalogos_requerimientos_rubros sol_req_det "
				+ "on (sol_req_det.solicitud_intervencion_requerimiento_id = sol_req.id) "
				+ "join fiscalizaciones_resultados res on "
				+ "(res.solicitud_intervencion_requerimiento_id = sol_req.id) "
				+ "join fiscalizaciones_resultados_detalles res_det on (res_det.fiscalizacion_resultado_id = res.id "
				+ "and res_det.catalogo_requerimiento_rubro_id = sol_req_det.id) "
				+ "left join situaciones_intervenciones sit on (sit.id = res_det.situacion_intervencion_id) "
				+ "join intervenciones_fiscalizadores i on sol.id = i.solicitud_intervencion_id "
				+ "where i.persona_id = :id_fiscalizador and sol_req.estado_obra_id in (2, 3, 4) "
				+ "order by res.fecha_intervencion, res_det.id");
		query.setParameter("id_fiscalizador", dataHolder.getUser().getId());

		List<Object[]> resultadoList = new ArrayList<Object[]>();
		try {
			resultadoList = query.getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return resultadoList;
	}

	/**
	 * Listado de anexos sin procesar
	 * 
	 * @return listado de anexos
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> getAnexosList() {
		Query query = em
				.createNativeQuery("select anexos.id, anexos.fiscalizacion_resultado_id, anexos.anexo_file_name "
						+ "from fiscalizaciones_resultados_anexos anexos "
						+ "join fiscalizaciones_resultados res on (res.id = anexos.fiscalizacion_resultado_id) "
						+ "join solicitudes_intervenciones_requerimientos sol_req on (sol_req.id = res.solicitud_intervencion_requerimiento_id) "
						+ "join solicitudes_intervenciones sol on (sol.id = sol_req.solicitud_intervencion_id) "
						+ "join intervenciones_fiscalizadores i on sol.id = i.solicitud_intervencion_id "
						+ "where i.persona_id = :id_fiscalizador and sol_req.estado_obra_id in (2, 3, 4) "
						+ "order by anexos.id, anexos.fiscalizacion_resultado_id");
		query.setParameter("id_fiscalizador", dataHolder.getUser().getId());

		List<Object[]> resultadoList = new ArrayList<Object[]>();
		try {
			resultadoList = query.getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return resultadoList;
	}

	/**
	 * Listado de situaciones sin procesar
	 * 
	 * @return listado de situaciones
	 */
	@SuppressWarnings("unchecked")
	public List<Object[]> getSituacionesList() {
		Query query = em.createNativeQuery("select id, descripcion from situaciones_intervenciones");
		List<Object[]> resultadoList = new ArrayList<Object[]>();
		try {
			resultadoList = query.getResultList();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return resultadoList;
	}

	/**
	 * Inicializa un Object[] a formato JSON
	 * 
	 * @param resultado
	 * @return un establecimiento en formato JSON generado a partir de una fila
	 *         del resultado de la consulta
	 */
	public JSONObject inicializarLocal(Object[] resultado) {
		JSONObject local = new JSONObject();
		local.put("id_establecimiento", resultado[0]);
		local.put("establecimiento", resultado[1]);
		return local;
	}

	/**
	 * Inicializa un Object[] a formato JSON
	 * 
	 * @param resultado
	 * @return un requerimiento en formato JSON generado a partir de una fila
	 *         del resultado de la consulta
	 */
	public JSONObject inicializarObra(Object[] resultado) {
		JSONObject obra = new JSONObject();
		obra.put("id_obra", resultado[2]);
		obra.put("obra", resultado[3]);
		obra.put("id_estado", resultado[4]);
		return obra;
	}

	/**
	 * Inicializa un Object[] a formato JSON
	 * 
	 * @param resultado
	 * @return un rubro en formato JSON generado a partir de una fila del
	 *         resultado de la consulta
	 */
	public JSONObject inicializarItem(Object[] resultado) {
		JSONObject item = new JSONObject();
		item.put("id_item", resultado[5]);
		item.put("item", resultado[6]);
		item.put("unidad_medida", resultado[7]);
		item.put("cantidad", resultado[8]);
		return item;
	}

	/**
	 * Inicializa un Object[] a formato JSON
	 * 
	 * @param resultado
	 * @return una fiscalizacion en formato JSON generado a partir de una fila
	 *         del resultado de la consulta
	 */
	public JSONObject inicializarAvance(Object[] resultado) {
		JSONObject avance = new JSONObject();
		avance.put("id_avance", resultado[0]);
		avance.put("solicitud_intervencion_id,", resultado[1]);
		avance.put("fecha_intervencion", resultado[2]);
		avance.put("longitud", resultado[3]);
		avance.put("latitud", resultado[4]);
		avance.put("id_obra", resultado[5]);
		avance.put("origen_app", resultado[6]);
		avance.put("numero_certificado", resultado[7]);
		return avance;
	}

	/**
	 * Inicializa un Object[] a formato JSON
	 * 
	 * @param resultado
	 * @return un avance en formato JSON generado a partir de una fila del
	 *         resultado de la consulta
	 */
	public JSONObject inicializarAvanceDet(Object[] resultado) {
		JSONObject avanceDet = new JSONObject();
		avanceDet.put("id_avance_det", resultado[8]);
		avanceDet.put("factor_intervencion", resultado[9]);
		avanceDet.put("observacion", resultado[10]);
		avanceDet.put("observacion_supervisor", resultado[11]);
		avanceDet.put("id_item", resultado[12]);
		avanceDet.put("id_situacion", resultado[13]);
		return avanceDet;
	}

	/**
	 * Inicializa un Object[] a formato JSON
	 * 
	 * @param resultado
	 * @return un anexo en formato JSON generado a partir de una fila del
	 *         resultado de la consulta
	 */
	public JSONObject inicializarAnexo(Object[] resultado) {
		JSONObject avance = new JSONObject();
		avance.put("id_anexo", resultado[0]);
		avance.put("id_avance", resultado[1]);
		avance.put("file", resultado[2]);
		return avance;
	}

	/**
	 * Solamente si la obra se encuentra en estado "iniciada" o "en curso" se
	 * pueden agregar fiscalizaciones
	 * 
	 * @param fiscalización
	 * @return boolean
	 */
	public boolean obraEnCurso(FiscalizacionesResultado fiscalizacion) {
		Query query = em.createNativeQuery("select 1 from solicitudes_intervenciones_requerimientos "
				+ "where id = :idObra and estado_obra_id > 1 and estado_obra_id < 7");
		query.setParameter("idObra", fiscalizacion.getSolicitudIntervencionRequerimientoId());
		try {
			if (query.getResultList().isEmpty())
				return false;
			else
				return true;
		} catch (NoResultException e) {
		}
		return false;
	}

	/* Guarda fiscalizaciones por obra */
	@SuppressWarnings("resource")
	public Response guardarFiscalizaciones(MultipartFormDataInput input) {
		try {
			// Obtenemos el array de fiscalizaciones
			Map<String, List<InputPart>> formParts = input.getFormDataMap();
			List<InputPart> fiscalizacionesInputPart = formParts.get("fiscalizaciones");
			FiscalizacionesResultado[] fiscalizacionResultados = null;
			if (fiscalizacionesInputPart != null) {
				Scanner s = null;
				try {
					InputPart inputPart = fiscalizacionesInputPart.get(0);
					InputStream istream = inputPart.getBody(InputStream.class, null);
					s = new Scanner(istream).useDelimiter("\\A");
					String fiscalizaciones = s.hasNext() ? s.next() : "";
					if (fiscalizaciones.equals(""))
						throw new IllegalArgumentException("No existen fiscalizaciones.");
					ObjectMapper mapper = new ObjectMapper();
					fiscalizacionResultados = mapper.readValue(fiscalizaciones, FiscalizacionesResultado[].class);
				} catch (IOException e) {
					e.printStackTrace();
					return Response.status(Status.BAD_REQUEST)
							.entity(ImmutableMap.of("error", "Error de datos.", "detail", e.getMessage())).build();
				} finally {
					if (s != null)
						s.close();
				}
			}

			// Validamos el array de fiscalizaciones
			Validate.isTrue(fiscalizacionResultados.length != 0, "No existen fiscalizaciones para ser agregadas.");

			// Obtenemos las fiscalizaciones que no serán actualizadas
			List<Integer> idObras = Arrays.asList(fiscalizacionResultados).stream()
					.map(f -> f.getSolicitudIntervencionRequerimientoId()).distinct().collect(Collectors.toList());
			List<Integer> idFiscalizaciones = Arrays.asList(fiscalizacionResultados).stream().filter(f -> f.getId() > 0)
					.map(f -> f.getId()).distinct().collect(Collectors.toList());
			if (idFiscalizaciones.isEmpty())
				idFiscalizaciones.add(Integer.MIN_VALUE);
			List<FiscalizacionesResultado> listadoFiscalizacionResultado = em
					.createQuery(QUERY_OBTENER_FISCALIZACIONES, FiscalizacionesResultado.class)
					.setParameter("idObras", idObras).setParameter("idFiscalizaciones", idFiscalizaciones)
					.getResultList();

			// Carga/Actualización de las fiscalizaciones
			for (FiscalizacionesResultado fiscalizacionResultado : fiscalizacionResultados) {
				Validate.notNull(fiscalizacionResultado, "No se puede agregar una fiscalización nula.");
				Validate.notNull(fiscalizacionResultado.getId(), "No se puede agregar una fiscalización con id nulo.");
				Validate.notNull(fiscalizacionResultado.getSolicitudIntervencionRequerimientoId(),
						"No se puede agregar una fiscalización con id de obra nulo.");

				if (fiscalizacionResultado.getId() > 0) // Actualizamos
					listadoFiscalizacionResultado.add(updateFiscalizacion(fiscalizacionResultado, input));
				else { // Cargamos si la obra es válida
					fiscalizacionResultado = addFiscalizacion(fiscalizacionResultado, input);
					if (fiscalizacionResultado != null)
						listadoFiscalizacionResultado.add(fiscalizacionResultado);
				}
			}

			// Eliminamos los anexos que vienen en borradas
			anexoLogic.eliminarAnexos(listadoFiscalizacionResultado, input);

			// Ordenadamos las fiscalizaciones por idObra, fechaIntervencion

			listadoFiscalizacionResultado
					.sort(Comparator.comparing(FiscalizacionesResultado::getSolicitudIntervencionRequerimientoId)
							.thenComparing(FiscalizacionesResultado::getFechaIntervencion));

			return Response.status(200).entity(listadoFiscalizacionResultado).build();

		} catch (Exception e) {
			e.printStackTrace();
			return Response.status(Status.BAD_REQUEST)
					.entity(ImmutableMap.of("error", "Error de datos.", "detail", e.getMessage())).build();
		}
	}

	public FiscalizacionesResultado addFiscalizacion(FiscalizacionesResultado fiscalizacion,
			MultipartFormDataInput input) {

		// Validamos la fecha de fiscalización
		SimpleDateFormat dt = new SimpleDateFormat("dd-MM-yyyyy");
		List<FiscalizacionesResultado> frTest = em
				.createQuery(QUERY_EXISTE_FISCALIZACION, FiscalizacionesResultado.class)
				.setParameter("idObra", fiscalizacion.getSolicitudIntervencionRequerimientoId())
				.setParameter("fechaIntervencion", fiscalizacion.getFechaIntervencion()).getResultList();
		Validate.isTrue(frTest.isEmpty(),
				"Ya existe una fiscalización en fecha " + dt.format(fiscalizacion.getFechaIntervencion()) + ".");

		// Validamos que se puede añadir la fiscalización a la obra
		if (!obraEnCurso(fiscalizacion))
			return null;

		Integer idLogico = fiscalizacion.getId();
		fiscalizacion.setId(null);
		fiscalizacion.setOrigenApp(true);
		fiscalizacion.setCreatedAt(new Timestamp(System.currentTimeMillis()));
		fiscalizacion.setUsuarioRegistroId(dataHolder.getUser().getUsuarioSeguridad().getId());

		// Cargamos los detalles
		if (fiscalizacion.getAvances() != null) {
			for (FiscalizacionesResultadosDetalle frd : fiscalizacion.getAvances()) {
				frd.setOrigenApp(true);
				frd.setUsuarioRegistroId(dataHolder.getUser().getUsuarioSeguridad().getId());
				frd.setCreatedAt(new Timestamp(System.currentTimeMillis()));
				frd.setFiscalizacion(fiscalizacion);
			}
		}

		// Cargamos las observaciones
		if (fiscalizacion.getObservaciones() != null) {
			for (FiscalizacionesObservaciones fo : fiscalizacion.getObservaciones()) {
				fo.setCreatedAt(new Timestamp(System.currentTimeMillis()));
				fo.setFiscalizacion(fiscalizacion);
			}
		}

		// Cargamos las verificaciones
		if (fiscalizacion.getVerificaciones() != null) {
			for (FiscalizacionesResultadosVerificaciones frv : fiscalizacion.getVerificaciones()) {
				frv.setCreatedAt(new Timestamp(System.currentTimeMillis()));
				frv.setFiscalizacion(fiscalizacion);
			}
		}

		fiscalizacion.setAnexos(new ArrayList<>());
		fiscalizacion = em.merge(fiscalizacion);
		fiscalizacion.setId_logico(idLogico);

		return anexoLogic.registrarAnexos(fiscalizacion, input);
	}

	public FiscalizacionesResultado updateFiscalizacion(FiscalizacionesResultado newFiscalizacion,
			MultipartFormDataInput input) {

		// Obtenemos la fiscalizacion
		FiscalizacionesResultado fiscalizacion = em.find(FiscalizacionesResultado.class, newFiscalizacion.getId());
		Validate.notNull(fiscalizacion, "La fiscalizacion con id " + newFiscalizacion.getId() + " no existe.");

		// Validamos la prioridad de la fiscalización
		Validate.notNull(newFiscalizacion.getUpdatedAt(), "La fecha de actualización de la fiscalización "
				+ fiscalizacion.getFechaIntervencion() + " no puede ser vacía.");
		Date toCompare = (fiscalizacion.getUpdatedAt() != null) ? fiscalizacion.getUpdatedAt()
				: new Date(Long.MIN_VALUE);
		if (toCompare.after(newFiscalizacion.getUpdatedAt()) || !obraEnCurso(newFiscalizacion)) {
			return fiscalizacion;
		} else {
			// Actualizamos los campos
			fiscalizacion.setOrigenApp(true);
			fiscalizacion.setUpdatedAt(new Timestamp(System.currentTimeMillis()));
			fiscalizacion.setLatitud(newFiscalizacion.getLatitud());
			fiscalizacion.setLongitud(newFiscalizacion.getLongitud());
			fiscalizacion.setObservacion(newFiscalizacion.getObservacion());
			fiscalizacion.setConclusion(newFiscalizacion.getConclusion());
			fiscalizacion.setControl(newFiscalizacion.getControl());

			// Actualizamos los detalles
			if (newFiscalizacion.getAvances() != null) {
				for (FiscalizacionesResultadosDetalle frd : newFiscalizacion.getAvances()) {
					if (frd.getId() == null) {
						frd.setOrigenApp(true);
						frd.setUsuarioRegistroId(dataHolder.getUser().getUsuarioSeguridad().getId());
						frd.setCreatedAt(new Timestamp(System.currentTimeMillis()));
						frd.setFiscalizacion(fiscalizacion);
						fiscalizacion.getAvances().add(frd);
					} else {
						Integer id = frd.getId();
						FiscalizacionesResultadosDetalle frd2 = fiscalizacion.getAvances().stream()
								.filter(x -> x.getId().equals(id)).findFirst()
								.orElse(new FiscalizacionesResultadosDetalle());
						frd2.setOrigenApp(true);
						frd2.setUpdatedAt(new Timestamp(System.currentTimeMillis()));
						frd2.setFactorIntervencion(frd.getFactorIntervencion());
					}
				}
			}

			// Actualizamos las observaciones
			if (newFiscalizacion.getObservaciones() != null) {
				for (FiscalizacionesObservaciones fo : newFiscalizacion.getObservaciones()) {
					if (fo.getId() == null) {
						fo.setCreatedAt(new Timestamp(System.currentTimeMillis()));
						fo.setFiscalizacion(fiscalizacion);
						fiscalizacion.getObservaciones().add(fo);
					} else if (fo.getId() > 0) {
						Integer id = fo.getId();
						FiscalizacionesObservaciones fo2 = fiscalizacion.getObservaciones().stream()
								.filter(x -> x.getId().equals(id)).findFirst()
								.orElse(new FiscalizacionesObservaciones());
						fo2.setObservacion(fo.getObservacion());
						fo2.setMedidaAdoptada(fo.getMedidaAdoptada());
						fo2.setUpdatedAt(new Timestamp(System.currentTimeMillis()));
					} else {
						fiscalizacion.getObservaciones().removeIf(f -> f.getId() == (fo.getId() * -1));
					}
				}
			}

			// Actualizamos las verificaciones
			if (newFiscalizacion.getVerificaciones() != null) {
				fiscalizacion.getVerificaciones().removeIf(f -> {
					for (FiscalizacionesResultadosVerificaciones f2 : newFiscalizacion.getVerificaciones()) {
						if (f2.getId() != null && f2.getId().equals(f.getId()))
							return false;
					}
					return true;
				});
				for (FiscalizacionesResultadosVerificaciones frv : newFiscalizacion.getVerificaciones()) {
					if (frv.getId() == null) {
						frv.setCreatedAt(new Timestamp(System.currentTimeMillis()));
						frv.setFiscalizacion(fiscalizacion);
						fiscalizacion.getVerificaciones().add(frv);
					} else {
						Integer id = frv.getId();
						FiscalizacionesResultadosVerificaciones frv2 = fiscalizacion.getVerificaciones().stream()
								.filter(x -> x.getId().equals(id)).findFirst()
								.orElse(new FiscalizacionesResultadosVerificaciones());
						frv2.setUpdatedAt(new Timestamp(System.currentTimeMillis()));
						frv2.setCumplimiento(frv.getCumplimiento());
						frv2.setValorDescriptivo(frv.getValorDescriptivo());
						frv2.setValorNumerico(frv.getValorNumerico());
					}
				}
			}

			newFiscalizacion.setAnexos(new ArrayList<>());
			return anexoLogic.registrarAnexos(em.merge(fiscalizacion), input);
		}

	}

	/* Asigna un certificado a la fiscalizacion */
	public Response generarCertificado(Integer id) {
		// Validamos la fiscalización a certificar
		Validate.notNull(id, "El id de fiscalización no puede ser nulo.");
		FiscalizacionesResultado fiscalizacionResultado = em.find(FiscalizacionesResultado.class, id);
		Validate.notNull(fiscalizacionResultado, "La fiscalización con id " + id + " no existe.");
		if (fiscalizacionResultado.getNumeroCertificacion() == null) {
			Date date = em.createQuery(QUERY_MAX_CERTIFICADO_FECHA, Date.class)
					.setParameter("idObra", fiscalizacionResultado.getSolicitudIntervencionRequerimientoId())
					.getSingleResult();
			if (date.after((fiscalizacionResultado.getFechaIntervencion())))
				throw new IllegalArgumentException("Ya existe una fiscalización certificada con fecha " + date);
			// Obtenemos el número de certificado para la fiscalización
			Integer numeroCertificacion = em.createQuery(QUERY_MAX_CERTIFICADO_NUMERO, Integer.class)
					.setParameter("idObra", fiscalizacionResultado.getSolicitudIntervencionRequerimientoId())
					.getSingleResult();
			fiscalizacionResultado.setNumeroCertificacion(numeroCertificacion);
			fiscalizacionResultado = em.merge(fiscalizacionResultado);
		}
		return Response.ok().entity(fiscalizacionResultado).build();
	}

}
