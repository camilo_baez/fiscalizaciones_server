package py.fiscalizaciones.managers;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang3.Validate;

import py.fiscalizaciones.domain.Usuario;

@Stateless
public class UsuarioLogic {

	//@formatter:off
	public static final String QUERY_GET_USUARIO = ""
			+ " SELECT u"
			+ " FROM "
			+ " 		Usuario u"
			+ " WHERE "
			+ " 		u.usuarioSeguridad.login = :username";
	//@formatter:on

	@PersistenceContext
	private EntityManager em;

	public Usuario getUsuarioByUsername(String username) {
		List<Usuario> toRet = em.createQuery(QUERY_GET_USUARIO, Usuario.class).setParameter("username", username)
				.getResultList();
		Validate.isTrue(!toRet.isEmpty(), "No existe el usuario dentro del sistema de fiscalizaciones.");
		return toRet.get(0);
	}

}
