package py.fiscalizaciones.managers;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Timestamp;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.Validate;
import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;

import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.AccessLevel;
import lombok.Getter;
import py.fiscalizaciones.domain.FiscalizacionesResultado;
import py.fiscalizaciones.domain.FiscalizacionesResultadosAnexo;
import py.framework.security.UserSessionDataHolder;
import py.framework.util.Config;
import py.framework.util.Utils;

@Stateless
@LocalBean
public class AnexoLogic {

	// @formatter:off
	public static final String QUERY_ANEXO_FIRMA = ""
			+ " SELECT fra "
			+ " FROM FiscalizacionesResultadosAnexo fra "
			+ " WHERE fra.fiscalizacion.id=:idFiscalizacion "
			+ " AND fra.anexoFileName=:fileName ";
	// @formatter:on

	@Inject
	@Getter(AccessLevel.PROTECTED)
	private Config config;

	@Inject
	UserSessionDataHolder dataHolder;

	@PersistenceContext(unitName = "fiscalizacion")
	private EntityManager em;

	/* Carga de registros */
	public FiscalizacionesResultado registrarAnexos(FiscalizacionesResultado fiscalizacion,
			MultipartFormDataInput input) {
		/*
		 * FORMATO:
		 * 
		 * data: file_<id_obra>_<id_avance> fileName: <id_anexo>
		 */

		// Definimos el string que agrupa los anexos por fiscalizacion
		Integer idFiscalizacion = (fiscalizacion.getId_logico() != null) ? fiscalizacion.getId_logico()
				: fiscalizacion.getId();
		String archivo = "file_" + fiscalizacion.getSolicitudIntervencionRequerimientoId() + "_" + idFiscalizacion;

		Map<String, List<InputPart>> formParts = input.getFormDataMap();
		List<InputPart> inPart = formParts.get(archivo);

		if (inPart != null) {
			String fileName, carpeta;
			Integer fileSize = 1, idLogico = null;
			boolean esFirma = false;
			FiscalizacionesResultadosAnexo anexo;
			for (InputPart inputPart : inPart) {
				try {
					InputStream istream = inputPart.getBody(InputStream.class, null);
					MultivaluedMap<String, String> headers = inputPart.getHeaders();
					fileName = Utils.parseFileName(headers);
					esFirma = (fileName.equals("fiscalizador") || fileName.equals("contratista"));

					// Formateamos los datos del anexo
					if (esFirma) {
						idLogico = null;
						List<FiscalizacionesResultadosAnexo> listAnexo = em
								.createQuery(QUERY_ANEXO_FIRMA, FiscalizacionesResultadosAnexo.class)
								.setParameter("idFiscalizacion", idFiscalizacion).setParameter("fileName", fileName)
								.getResultList();
						if (!listAnexo.isEmpty()) {
							anexo = listAnexo.get(0);
							anexo.setAnexoUpdatedAt(new Timestamp(System.currentTimeMillis()));
							anexo.setUpdatedAt(new Timestamp(System.currentTimeMillis()));
						} else {
							anexo = new FiscalizacionesResultadosAnexo();
							anexo.setAnexoContentType("application/octet-stream");
							anexo.setId(null);
							anexo.setAnexoCreatedAt(new Timestamp(System.currentTimeMillis()));
							anexo.setCreatedAt(new Timestamp(System.currentTimeMillis()));
							anexo.setAnexoFileName(fileName);
							anexo.setAnexoFileSize(fileSize);
							anexo.setAnexoUpdatedAt(new Timestamp(System.currentTimeMillis()));
							anexo.setUsuarioRegistroId(dataHolder.getUser().getUsuarioSeguridad().getId());
							anexo.setTipoAdjunto(9);
							anexo.setFiscalizacion(fiscalizacion);
							anexo = em.merge(anexo);
						}

					} else {
						idLogico = Integer.valueOf(fileName);
						anexo = new FiscalizacionesResultadosAnexo();
						anexo.setAnexoContentType("image/jpg");
						anexo.setId(null);
						anexo.setAnexoCreatedAt(new Timestamp(System.currentTimeMillis()));
						anexo.setCreatedAt(new Timestamp(System.currentTimeMillis()));
						anexo.setAnexoFileName(fileName);
						anexo.setAnexoFileSize(fileSize);
						anexo.setAnexoUpdatedAt(new Timestamp(System.currentTimeMillis()));
						anexo.setUsuarioRegistroId(dataHolder.getUser().getUsuarioSeguridad().getId());
						anexo.setTipoAdjunto(8);
						anexo.setFiscalizacion(fiscalizacion);
						anexo = em.merge(anexo);
						anexo.setAnexoFileName(anexo.getId() + ".jpg");
					}

					// Guardamos en el filesystem
					carpeta = config.getUploadFolder() + anexo.getId() + config.getSeparador();
					Utils.verificarCarpeta(carpeta);
					fileSize = Utils.saveFile(istream, carpeta + anexo.getAnexoFileName());

					// Actualizamos tamaño del anexo
					anexo.setAnexoFileSize(fileSize);
					anexo = em.merge(anexo);

					// Si es anexo, el idLogico es distinto de nulo
					anexo.setId_logico(idLogico);

					// Agregamos al listado de anexos
					fiscalizacion.getAnexos().add(anexo);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return fiscalizacion;
	}

	/* Eliminación de anexos por medio de array de Ids */
	@SuppressWarnings("resource")
	public void eliminarAnexos(List<FiscalizacionesResultado> listadoFiscalizacionResultado,
			MultipartFormDataInput input) throws Exception {
		Map<String, List<InputPart>> formParts = input.getFormDataMap();
		List<InputPart> inPart = formParts.get("borradas");
		Integer[] borradas = null;
		if (inPart != null) {
			// Obtenemos el bloque
			Scanner s = null;
			try {
				InputPart inputPart = inPart.get(0);
				InputStream istream = inputPart.getBody(InputStream.class, null);
				s = new Scanner(istream).useDelimiter("\\A");
				String borradasParse = s.hasNext() ? s.next() : "";
				if (borradasParse.equals(""))
					return;
				ObjectMapper mapper = new ObjectMapper();
				borradas = mapper.readValue(borradasParse, Integer[].class);
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if (s != null)
					s.close();
			}
			// Logica de borrado
			if (borradas.length != 0) {
				for (Integer idAnexo : borradas) {
					FiscalizacionesResultadosAnexo fra = em.find(FiscalizacionesResultadosAnexo.class, idAnexo);
					Validate.notNull(fra, "El anexo " + idAnexo + " no existe.");
					String path = config.getUploadFolder() + fra.getId();
					File file = new File(path);
					if (file.isDirectory()) {
						// Borramos de base de datos
						em.remove(fra);
						// Borramos del listado a retornar
						for (FiscalizacionesResultado f : listadoFiscalizacionResultado) {
							if (f.getAnexos() != null) {
								for (FiscalizacionesResultadosAnexo fa : f.getAnexos()) {
									if (fa.getId() == idAnexo) {
										f.getAnexos().remove(fa);
										break;
									}
								}
							}
						}
						// Borramos del filesystem
						FileUtils.deleteDirectory(file);
					} else
						throw new IllegalArgumentException(
								"El anexo " + idAnexo + " no se encuentra almacenado en el sistema.");
				}
			}
		}
	}

	/* Descarga de fotos individual por Id */
	public Response descargarAnexo(Integer id) {
		// Validamos el anexo a descargar
		Validate.notNull(id, "El id de anexo no puede ser nulo.");
		FiscalizacionesResultadosAnexo anexo = em.find(FiscalizacionesResultadosAnexo.class, id);
		Validate.notNull(anexo, "El anexo con id " + id + " no existe.");

		String carpeta = config.getUploadFolder() + id + config.getSeparador();
		String nombreFoto = anexo.getAnexoFileName();
		String filePath = carpeta + nombreFoto;

		File file = new File(filePath);
		FileInputStream fileInputStream;
		try {
			fileInputStream = new FileInputStream(file);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException("Error al descargar el anexo.", e);
		}
		ResponseBuilder responseBuilder = Response.ok((Object) fileInputStream);
		responseBuilder.type("image/jpg");
		responseBuilder.header("Content-Disposition", "filename=image.jpg");
		return responseBuilder.build();
	}

}
