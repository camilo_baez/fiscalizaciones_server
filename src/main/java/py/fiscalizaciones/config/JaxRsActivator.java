package py.fiscalizaciones.config;

import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import py.fiscalizaciones.rest.EjbExceptionMapper;
import py.fiscalizaciones.rest.IllegalArgumentExceptionMapper;
import py.fiscalizaciones.rest.IllegalStateExceptionMapper;
import py.fiscalizaciones.rest.NullPointerExceptionMapper;
import py.fiscalizaciones.rest.Rest;
import py.fiscalizaciones.rest.ValidationExceptionMapper;
import py.framework.security.AuthenticationEndpoint;
import py.framework.security.AuthenticationFilter;

/**
 * A class extending {@link Application} and annotated with @ApplicationPath is
 * the Java EE 6 "no XML" approach to activating JAX-RS.
 * 
 * <p>
 * Resources are served relative to the servlet path specified in the
 * {@link ApplicationPath} annotation.
 * </p>
 */
@ApplicationPath("/rest")
public class JaxRsActivator extends Application {

	HashSet<Object> singletons = new HashSet<>();

	public JaxRsActivator() {
		// Cambia el idioma de la JVM
		Locale.setDefault(new Locale("es", "PY"));
	}

	@Override
	public Set<Class<?>> getClasses() {
		HashSet<Class<?>> set = new HashSet<>();
		set.add(ObjectMapperProvider.class);
		set.add(AuthenticationEndpoint.class);
		// Comentar esta linea para que no exista seguridad!
		set.add(AuthenticationFilter.class);
		set.add(Rest.class);
		set.add(IllegalArgumentExceptionMapper.class);
		set.add(NullPointerExceptionMapper.class);
		set.add(IllegalStateExceptionMapper.class);
		set.add(EjbExceptionMapper.class);
		set.add(ValidationExceptionMapper.class);
		return set;
	}

	@Override
	public Set<Object> getSingletons() {
		return singletons;
	}

}