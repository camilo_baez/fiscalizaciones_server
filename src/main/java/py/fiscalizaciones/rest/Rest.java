package py.fiscalizaciones.rest;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;

import py.fiscalizaciones.managers.AnexoLogic;
import py.fiscalizaciones.managers.Manager;

@Stateless
@Path("/")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class Rest {

	// inyeccion del manager que actua de controlador
	@Inject
	Manager em;

	@Inject
	AnexoLogic anexoLogic;

	/**
	 * Obtiene toda la informacion (locales, obras, items, fiscalizaciones,
	 * detalles, anexos, informes)
	 * 
	 * @return locales y situaciones
	 */
	@GET
	@Path("/fiscalizaciones")
	public Response obtenerDatos() {
		return em.obtenerDatos();
	}

	/*
	 * @GET
	 * 
	 * @Path("/fiscalizaciones2") public Response obtenerDatosAnterior() {
	 * return em.obtenerDatosAnterior(); }
	 */

	/**
	 * Guarda listado de Fiscalizaciones de una obra
	 * 
	 * @param fisca
	 * @return fiscalizaciones de la obra actualizadaa
	 */
	@PUT
	@Path("/fiscalizaciones")
	@Consumes("multipart/form-data")
	public Response guardarFiscalizaciones(MultipartFormDataInput input) {
		return em.guardarFiscalizaciones(input);
	}

	/**
	 * Genera el certificado de obras
	 * 
	 * @param id
	 * @param fiscalizacion
	 * @return el certificado en formato pdf (o el enlace)
	 */
	@PUT
	@Path("/fiscalizaciones/{id}/certificado")
	public Response generarCertificado(@PathParam("id") Integer id) {
		return em.generarCertificado(id);
	}

	/**
	 * Descargar fotos por id
	 * 
	 * @param id
	 * @return el informe en formato pdf (o el enlace)
	 */
	@GET
	@Path("/fiscalizaciones/anexos/{id}")
	@Produces({ "image/jpg" })
	public Response descargarAnexo(@PathParam("id") Integer id) {
		return anexoLogic.descargarAnexo(id);
	}

}
