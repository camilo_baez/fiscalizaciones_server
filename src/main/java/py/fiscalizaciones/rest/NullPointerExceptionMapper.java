package py.fiscalizaciones.rest;

import java.util.Collections;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class NullPointerExceptionMapper implements ExceptionMapper<NullPointerException> {

	@Override
	public Response toResponse(NullPointerException exception) {
		return buildResponse(exception);
	}

	public static Response buildResponse(NullPointerException exception) {
		if (exception.getMessage() == null || exception.getMessage().isEmpty())
			exception.printStackTrace();
		return Response.status(Status.BAD_REQUEST).entity(Collections.singletonMap("error", exception.getMessage()))
				.type(MediaType.APPLICATION_JSON).build();
	}

}
