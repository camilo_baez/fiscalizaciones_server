package py.fiscalizaciones.rest;

import java.util.Collections;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class IllegalArgumentExceptionMapper implements ExceptionMapper<IllegalArgumentException> {

	@Override
	public Response toResponse(IllegalArgumentException exception) {
		return buildResponse(exception);
	}

	public static Response buildResponse(IllegalArgumentException exception) {
		exception.printStackTrace();
		return Response.status(Response.Status.BAD_REQUEST).type(MediaType.APPLICATION_JSON)
				.entity(Collections.singletonMap("error", exception.getMessage())).build();
	}

}