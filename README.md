AGREGAR DATA SOURCE AL JBOSS

<datasource jta="false" jndi-name="java:/seccional" pool-name="seccional" enabled="true" use-java-context="true" use-ccm="false">
                    <connection-url>jdbc:postgresql://localhost:5432/seccional</connection-url>
                    <driver-class>org.postgresql.Driver</driver-class>
                    <driver>postgresql</driver>
                    <pool>
                        <min-pool-size>2</min-pool-size>
                        <max-pool-size>20</max-pool-size>
                        <flush-strategy>IdleConnections</flush-strategy>
                    </pool>
                    <security>
                        <user-name>postgres</user-name>
                        <password>postgres</password>
                    </security>
                    <validation>
                        <check-valid-connection-sql>SELECT 1</check-valid-connection-sql>
                        <background-validation>true</background-validation>
                        <background-validation-millis>60000</background-validation-millis>
                    </validation>
                    <statement>
                        <prepared-statement-cache-size>0</prepared-statement-cache-size>
                        <share-prepared-statements>false</share-prepared-statements>
                    </statement>
                </datasource>
